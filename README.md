## **Overview**

This is a **Vulnerability Management Solution** (VMS) agent that will gather the installed packages on your server/pc and reformat to a **CPE** format and send it to the specified server for later manipulation.

## **Features**
- [x] Packages Gathering
- [x] CPE Formation
- [x] Server Token Authentication
- [x] Server Credential Authentication
- [x] Local Data


## **Getting started**
* To get started for the first time run VMS Agent Setup file.
* Proceed through installation.
* Enter the credentials required.
* Setup the routine on which the agent will run on.
* Wait for the installation to conclude.
* And you're Golden


#### for a fresh start do as following:
*run the setup file.
*select modify.
*renter your credentials and execution routine. 
* Wait for the installation to conclude.
* And you're Golden

> NEGIN PARDAZESH FARDA
