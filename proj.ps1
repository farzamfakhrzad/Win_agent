$env:computername
                $os = ((gwmi win32_operatingsystem).caption) | Out-String
                write-host $os
                write-host $env
                $array = @()
                $computername=$env
                $UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
                $reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$computername)
                $regkey=$reg.OpenSubKey($UninstallKey)
                $subkeys=$regkey.GetSubKeyNames()
                foreach($key in $subkeys){
                    $thisKey=$UninstallKey+"\\"+$key
                    $thisSubKey=$reg.OpenSubKey($thisKey)
                    $obj = New-Object PSObject
                    $obj | Add-Member -MemberType NoteProperty -Name "ComputerName" -Value $computername
                    $obj | Add-Member -MemberType NoteProperty -Name "DisplayName" -Value $($thisSubKey.GetValue("DisplayName"))
                    $obj | Add-Member -MemberType NoteProperty -Name "DisplayVersion" -Value $($thisSubKey.GetValue("DisplayVersion"))
                    $obj | Add-Member -MemberType NoteProperty -Name "InstallLocation" -Value $($thisSubKey.GetValue("InstallLocation"))
                    $obj | Add-Member -MemberType NoteProperty -Name "Publisher" -Value $($thisSubKey.GetValue("Publisher"))
                    $array += $obj
                }
                $array | Where-Object { $_.DisplayName } | select  DisplayName, DisplayVersion, Publisher |Export-Clixml  hkml.xml
                Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate|Export-Clixml Regkey.xml
