import json
import base64
from Crypto.Cipher import AES
def decrypt( enc, unpad):
    private_key = b'\xe9:\x17\x8d\xd9\x9a\xbf0\xb6\xc1\t\x93\x12|0+1\x9b\xfaU}+\x06\xa1\xe9\xe7\x87\r\xe6\x15\x9d\x01'
    enc = base64.b64decode(enc)
    iv = 16 * '\x00'.encode()
    cipher = AES.new(private_key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))
with open("cred.json", "r") as json_data:
    unpad = lambda s: s[:-ord(s[len(s) - 1:])]
    credential_content = json_data.read()
    dec = decrypt(credential_content, unpad).decode()
    data = json.loads(dec)
    print(data)