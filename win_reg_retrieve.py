import os
import subprocess
import sys
import socket
import logging
from time import sleep

import winreg
import csv
import base64
from Crypto.Cipher import AES
from colorama import Fore
from urllib import parse as urlparseutils
import requests
import json
import datetime

logging.basicConfig(filename='agent-logs.log', filemode="a+",
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

NAME = "name"
PUBLISHER = "publisher"
VERSION = "version"


class WinAgent:

    def __init__(self):
        self.software_list = []
        self.os_info = {}
        self.extras = {}
        self.hotfixes = ""
        if len(sys.argv) == 1:
            self.credential_file_path = os.path.join(os.path.dirname(__file__), "cred.json")
        elif len(sys.argv) == 2:
            self.credential_file_path = sys.argv[1]


    @classmethod
    def read_registry_value(cls, root, key_str, query_for):
        # todo: log for error handling
        try:
            aReg = winreg.ConnectRegistry(None, root)
        except:
            print(f"Could not access registry.")
            return None
        try:
            aKey = winreg.OpenKey(aReg, key_str)
        except:
            print(f"can not open registry key `{key_str}`")
            return None
        try:
            result = winreg.QueryValueEx(aKey, query_for)
            value = result[0]
            aKey.Close()
            return value
        except:
            print(f"Could not read value of `{query_for}` from `{key_str}`")
            return None

    @classmethod
    def read_registry_value_for_local_and_current_user(cls, key_str, query_for):
        value = cls.read_registry_value(winreg.HKEY_LOCAL_MACHINE, key_str, query_for)
        if value is not None:
            return value
        else:
            return cls.read_registry_value(winreg.HKEY_CURRENT_USER, key_str, query_for)

    @classmethod
    def get_iis_version(cls):
        key_str = "SOFTWARE\\Microsoft\\InetStp"
        majorVersion = cls.read_registry_value_for_local_and_current_user(key_str, "MajorVersion")
        minorVersion = cls.read_registry_value_for_local_and_current_user(key_str, "MinorVersion")
        if majorVersion is None:
            return None
        return {
            NAME: "internet_information_server",
            PUBLISHER: "microsoft",
            VERSION: f"{majorVersion}.{minorVersion}"
        }

    def get_net_framework_above45(self):
        try:
            aReg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
        except OSError:
            print(f'Could not open registry key {str(winreg.HKEY_LOCAL_MACHINE)}')
            return []
        subkey = "SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full"
        try:
            aKey = winreg.OpenKey(aReg, subkey)
        except OSError:
            print("can not open registry key "
                  "`HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full.`")
            return []

        dot_net_version = []

        try:
            result = winreg.QueryValueEx(aKey, "Release")
            releaseKey = result[0]
            dot_net_version.append({
                NAME: ".net_framework",
                PUBLISHER: "microsoft",
                VERSION: str(releaseKey)
            })
        except Exception:
            print("Could not read register value `Release` of registry key"
                  "`HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full.`")

        return dot_net_version

    def get_net_framework_below45(self):
        net_frameworks = []
        # .net_framework 1.0
        akey = "Software\\Microsoft\\.NETFramework\\Policy\\v1.0\\3705"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "1.0"
            })

        # .net_framework 1.1
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v1.1.4322"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "1.1"
            })

        # .net_framework 2.0
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v2.0.50727"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "2.0"
            })

        # .net_framework 3.0
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'InstallSuccess')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "3.0"
            })
        # .net_framework 3.5
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v3.5"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "3.5"
            })
        # .net_framework 4.0 client
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "4.0"
            })

        # .net_framework 4.0 Full
        akey = "Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full"
        value = self.__class__.read_registry_value_for_local_and_current_user(akey, 'Install')
        if value == 1:
            net_frameworks.append({
                PUBLISHER: "microsoft", NAME: ".net_framework", VERSION: "4.0"
            })

        return net_frameworks

    @staticmethod
    def read_registery(hive, flag):
        # todo logs for error handling
        try:
            aReg = winreg.ConnectRegistry(None, hive)
        except OSError as e:
            print(f"Could not access registry {str(hive)}")
            return []
        try:
            aKey = winreg.OpenKey(aReg, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
                                  0, winreg.KEY_READ | flag)
        except OSError as e:
            print("Could not Open registry key "
                  "`\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall`")
            return []

        count_subkey = winreg.QueryInfoKey(aKey)[0]

        software_list = []

        for i in range(count_subkey):
            software = {}
            try:
                asubkey_name = winreg.EnumKey(aKey, i)
                asubkey = winreg.OpenKey(aKey, asubkey_name)
                software[NAME] = winreg.QueryValueEx(asubkey, "DisplayName")[0]

                try:
                    software[VERSION] = winreg.QueryValueEx(asubkey, "DisplayVersion")[0]
                except EnvironmentError:
                    software[VERSION] = None
                try:
                    software[PUBLISHER] = winreg.QueryValueEx(asubkey, "Publisher")[0]
                except EnvironmentError:
                    software[PUBLISHER] = None
                software_list.append(software)
            except EnvironmentError:
                continue

        return software_list

    @classmethod
    def get_os_info(cls):
        winnt_key = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"
        currentBuildNumber = cls.read_registry_value(winreg.HKEY_LOCAL_MACHINE, winnt_key, 'CurrentBuildNumber')
        if int(currentBuildNumber) > 10000:
            installationType = cls.read_registry_value(winreg.HKEY_LOCAL_MACHINE, winnt_key, 'InstallationType')
            releaseId = cls.read_registry_value(winreg.HKEY_LOCAL_MACHINE, winnt_key, "ReleaseId")
            return {
                "type": str(installationType),
                "build_number": str(currentBuildNumber),
                "release_id": str(releaseId)
            }
        else:
            product_name = cls.read_registry_value(winreg.HKEY_LOCAL_MACHINE, winnt_key, 'ProductName')
            return {
                "build_number": str(currentBuildNumber),
                "product_name": str(product_name)
            }

    @classmethod
    def get_hotfix(cls):
        proccess = subprocess.Popen([r'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe',
                                     '-ExecutionPolicy',
                                     'Unrestricted',
                                     "GET-HOTFIX"],
                                    stdout=subprocess.PIPE)
        hotfix = proccess.communicate()[0].decode("utf-8").strip()

        return hotfix

    def setup(self):
        print("VMS Agent has begun it's process, please wait...")

        # read default registry keys
        self.software_list += self.__class__.read_registery(winreg.HKEY_LOCAL_MACHINE, winreg.KEY_WOW64_32KEY) + \
                              self.__class__.read_registery(winreg.HKEY_LOCAL_MACHINE, winreg.KEY_WOW64_64KEY) + \
                              self.__class__.read_registery(winreg.HKEY_CURRENT_USER, 0)

        # read .net_framework >= 4.5
        self.software_list += self.get_net_framework_above45()
        # read .net_frameword < 4.5
        self.software_list += self.get_net_framework_below45()
        # read iis info
        iis = self.__class__.get_iis_version()
        if iis is not None:
            self.software_list.append(iis)
        print('Number of installed apps: %s' % len(self.software_list))

        # read os info
        self.os_info = self.__class__.get_os_info()

        self.hotfixes = self.__class__.get_hotfix()

        self.extras = self.retrieve_extra_data(self.get_extra_keys())

    @property
    def local_file_name(self):
        current_time = datetime.datetime.now()
        computer_name = socket.gethostname()
        local_file_name = computer_name + "#" + current_time.strftime("%Y-%m-%d")
        return local_file_name

    def to_csv(self, path=None):
        if path is None:
            path = os.path.join(os.path.dirname(__file__), self.local_file_name + ".csv")
        keys = self.software_list[0].keys()
        with open(path, 'w', encoding='utf-8') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(self.software_list)

    def decrypt(self, raw_encrypted_data):
        unpad = lambda s: s[:-ord(s[len(s) - 1:])]
        private_key = b'\xe9:\x17\x8d\xd9\x9a\xbf0\xb6\xc1\t\x93\x12|0+1\x9b\xfaU}+\x06\xa1\xe9\xe7\x87\r\xe6\x15\x9d\x01'
        base64_data = base64.b64decode(raw_encrypted_data)
        iv = 16 * '\x00'.encode()
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(base64_data[16:]))

    def get_cred_data(self):
        try:
            with open(self.credential_file_path, "r") as cred_data:
                d_content = cred_data.read()
                decrypted_data = self.decrypt(d_content).decode()
                cred_dic = json.loads(decrypted_data)
                payload = {
                    "username": cred_dic["username"].replace("\\u0000", ""),
                    "password": cred_dic["password"].replace("\\u0000", "")
                }
                device_ip = cred_dic['ip'].replace("\\u0000", "")
                vms_address = cred_dic['address'].replace("\\u0000", "")
                return payload, device_ip, vms_address
        except:
            logging.error(f"Could not open credential file, it should be at: {self.credential_file_path}")
            sys.exit(-1)

    def authenticate(self, vms_address, payload):

        try:
            vms_url = urlparseutils.urlunparse(
                ("http", vms_address, "/", "", "", "")
            )
            try:
                vms_url = requests.get(vms_url).url
            except:
                vms_url = requests.get(vms_url.replace("http", "https"), verify=False).url

            token_url = urlparseutils.urljoin(vms_url, 'api/token/')
            token_response = requests.post(token_url,
                                           json=payload,
                                           verify=False)

            if token_response.status_code == 200:
                authentication_response = token_response.json()
            else:
                error_text = f"something went wrong, VMS respond with status code: {token_response.status_code}"
                print(Fore.RED + error_text)
                logging.critical(error_text)
                logging.critical(token_response.text)
                sys.exit(-1)
        except requests.exceptions.ConnectionError as e:
            logging.critical("Check your Internet connection or Credentials")
            print(Fore.RED + "An Error Encountered, Please check the log file in Program Folder for more info")
            sys.exit(-1)

        agent_header = {'Authorization': 'Bearer {}'.format(authentication_response["access"])}

        return agent_header, token_url

    def send_data(self):

        payload, device_ip, vms_address = self.get_cred_data()

        auth_header, token_url = self.authenticate(vms_address=vms_address, payload=payload)

        uri = urlparseutils.urlparse(token_url)
        agent_url = urlparseutils.urlunparse(
            (uri.scheme, uri.netloc, "api/agent/v3/win-agent/", "", "", "")
        )

        data = {
            "os_info": self.os_info,
            "software_list": self.software_list,
            "hotfixes": self.hotfixes,
            "device_info": {
                "ip": device_ip,
                "hostname": socket.gethostname()
            },
            "extras": self.extras
        }

        # print(json.dumps(data))
        # print(json.dumps(data, indent=2))

        agent_call_response = requests.post(agent_url, json=data, headers=auth_header, verify=False)
        if agent_call_response.status_code == 200:
            logging.info(json.dumps(agent_call_response.json()))
            return True, agent_call_response.json()
        else:
            logging.critical(agent_call_response.text)
            return False, agent_call_response.text

    def get_extra_keys(self):
        payload, device_ip, vms_address = self.get_cred_data()

        auth_header, token_url = self.authenticate(vms_address=vms_address, payload=payload)

        uri = urlparseutils.urlparse(token_url)
        agent_url = urlparseutils.urlunparse(
            (uri.scheme, uri.netloc, "api/agent/v3/win-agent-extras/", "", "", "")
        )

        extras_response = requests.get(agent_url, headers=auth_header, verify=False)

        if extras_response.status_code == 200:
            logging.info("retrieved extras from vms server")
            return extras_response.json()
        else:
            logging.info("could not retrieve extras from vms server")
            logging.error(extras_response.text)
            return {}

    def get_extra_registry(self, query_dict):
        root = getattr(winreg, query_dict['root'])
        path = query_dict['path']
        query_for = query_dict['query_for']
        results = {}
        for each in query_for:
            try:
                results[each] = self.read_registry_value(root, path, each)
            except Exception as e:
                results[each] = str(e)
        return results

    def get_extra_run_cmd(self, ps_dict):
        cmd = ps_dict['cmd']
        try:
            prc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf8')
            stdout, stderr = prc.communicate()
        except Exception as e:
            stdout = ""
            stderr = str(e)
        return {
            'stdout': stdout,
            'stderr': stderr
        }

    def retrieve_extra_data(self, extras):
        for key, value in extras.items():
            if key == 'read_registry':
                results = self.get_extra_registry(value)
                value.update(results=results)
            elif key == 'run_cmd':
                results = self.get_extra_run_cmd(value)
                value.update(results=results)
            else:
                self.retrieve_extra_data(value)
        return extras


if __name__ == '__main__':
    win_agent = WinAgent()
    win_agent.setup()
    win_agent.to_csv()
    status, vms_response = win_agent.send_data()
    if status:
        print(Fore.GREEN, "DATA SENT SUCCESSFULLY.")
        sleep(5)
    else:
        print(Fore.RED, "COULD NOT SEND DATA!\nPlease Contact Support")
        sleep(10)

