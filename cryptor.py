import base64
from Crypto.Cipher import AES
import json
import sys
def encrypt( raw, pad):
    private_key = b'\xe9:\x17\x8d\xd9\x9a\xbf0\xb6\xc1\t\x93\x12|0+1\x9b\xfaU}+\x06\xa1\xe9\xe7\x87\r\xe6\x15\x9d\x01'
    raw = pad(raw)
    iv = 16 * '\x00'.encode()
    cipher = AES.new(private_key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw.encode()))
def create_encrypted_credentials_file(path, credentials, pad):
    with open(path, "w") as json_credential_file:
            content = {"address": credentials[0], "username": credentials[2],"password": credentials[3],"ip": credentials[1]}
            json_content = json.dumps(content)
            enc = encrypt(json_content, pad)
            json_credential_file.write(enc.decode())
BLOCK_SIZE = 16
credentials=[sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]]
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
create_encrypted_credentials_file(sys.argv[1],credentials,pad)
